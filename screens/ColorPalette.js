import React from "react"
import { StyleSheet, FlatList } from "react-native"

import BoxColor from "../components/BoxColor"

const ColorPallete = ({ route }) => {
  return (
    <FlatList
      style={style.container}
      data={route.params.colors}
      keyExactor={(item) => item}
      renderItem={({ item }) => (
        <BoxColor text={item.colorName} code={item.hexCode} />
      )}
    />
  )
}

const style = StyleSheet.create({
  container: {
    paddingTop: 10,
    paddingHorizontal: 10,
    backgroundColor: "white",
  },
  title: {
    fontSize: 18,
    fontWeight: "bold",
    marginBottom: 10,
  },
})

export default ColorPallete
