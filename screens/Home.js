import React, { useState, useCallback, useEffect } from "react"
import { FlatList, StyleSheet, Text } from "react-native"
import { TouchableOpacity } from "react-native-gesture-handler"

import PalettePreview from "../components/PalettePreview"

const Home = ({ navigation, route }) => {
  const newPallete = route.params ? route.params.newPalette : undefined

  const [palettes, setPalettes] = useState([])
  const [isRefreshing, setIsRefreshing] = useState(false)

  const handleFetchPalettes = useCallback(async () => {
    const result = await fetch(
      "https://color-palette-api.kadikraman.vercel.app/palettes"
    )
    if (result.ok) {
      const data = await result.json()
      setPalettes(data)
    }
  }, [])

  useEffect(() => {
    handleFetchPalettes()
  }, [])

  const handleRefresh = useCallback(async () => {
    setIsRefreshing(true)
    await handleFetchPalettes()
    setTimeout(() => {
      setIsRefreshing(false)
    }, 1000)
  })

  useEffect(() => {
    if (newPallete) {
      setPalettes((palettes) => [newPallete, ...palettes])
    }
  }, [newPallete])

  return (
    <FlatList
      style={styles.list}
      data={palettes}
      refreshing={isRefreshing}
      onRefresh={() => {
        handleRefresh()
      }}
      keyExtractor={(item) => item.paletteName}
      renderItem={({ item }) => (
        <PalettePreview
          handlePress={() => navigation.navigate("ColorPalette", item)}
          colorPalette={item}
        />
      )}
      ListHeaderComponent={() => (
        <TouchableOpacity
          onPress={() => navigation.navigate("AddNewPaletteModal")}
        >
          <Text style={styles.newButton}>Add New Palette</Text>
        </TouchableOpacity>
      )}
    />
  )
}

const styles = StyleSheet.create({
  list: {
    padding: 10,
    backgroundColor: "white",
  },
  newButton: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#00a0b0",
    marginBottom: 10
  },
})

export default Home
