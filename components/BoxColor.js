import React from "react"
import { StyleSheet, Text, View } from "react-native"

const BoxColor = ({ text, code }) => {
  const boxColor = {
    backgroundColor: code,
  }

  const textColor = {
    color:
      parseInt(code.replace("#", ""), 16) > 0xffffff / 1.1
        ? "black"
        : "white",
  }

  return (
    <View style={[styles.box, boxColor]}>
      <Text style={[styles.boxText, textColor]}>
        {text}: {code}
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  boxText: {
    color: "white",
    fontWeight: "bold",
  },
  box: {
    marginBottom: 10,
    padding: 10,
    justifyContent: "center",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 1,
    elevation: 2,
  },
})

export default BoxColor
